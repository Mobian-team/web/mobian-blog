+++
title = "2025: off to a flying start!"
date = 2025-02-19T00:00:00+00:00
tags = ["mobian", "overview", "roadmap"]
+++

Over the past few months, and especially since the last holiday season, many
exciting things have happened in Mobian: new devices are (about to be)
officially supported, many new and improved packages have made their way into
both Debian and Mobian, and we're getting ready for our next stable release!

<!--more-->

### Towards a "device-specific only" repository

Thanks to the involvement of upstream developers (by either accepting or
writing patches to ensure their applications can be used on mobile devices)
and the work of the relevant Debian teams, many GNOME and KDE apps were made
adaptive and could be uploaded to the main Debian archive without any
additional downstream patch.

This allowed us to drop (almost) all of the applications we were distributing
through the Mobian package repository:
* [Evince](https://apps.gnome.org/Evince/) has been replaced with
  [Papers](https://apps.gnome.org/Papers/), [Loupe](https://apps.gnome.org/Loupe/)
  is now used instead of [EOG](https://wiki.gnome.org/Apps/EyeOfGnome) and our
  main camera app is now [Snapshot](https://apps.gnome.org/Snapshot/), all
  available directly from Debian!
* [GNOME Text Editor](https://apps.gnome.org/TextEditor/) now works well enough
  on mobile devices, as do [Software](https://apps.gnome.org/Software/) and
  [Settings](https://apps.gnome.org/Settings/)

We plan on getting rid of the remaining few ones before the
[Trixie release](https://lists.debian.org/debian-devel-announce/2025/01/msg00004.html):
* [Millipixels](https://source.puri.sm/Librem5/millipixels) (Librem 5 camera
  app) will be replaced by [Megapixels](https://gitlab.com/megapixels-org/Megapixels)
  v2 once it hits the Debian archive
* [wake-mobile](https://gitlab.gnome.org/kailueke/wake-mobile) will likely be
  dropped as it's no longer maintained, and upstream offers
  [.deb packages](https://gitlab.gnome.org/kailueke/wake-mobile/-/releases) for
  download anyway

Except for a patched version of GTK 3 which we will keep maintaining until
[Phosh](https://phosh.mobi) is ported to GTK 4, this means all packages in the
Mobian archive will be either Mobian-specific (mainly metapackages) or hardware
support packages (kernels and device-specific tweaks).

### A better ~~tomorrow~~ greeter

Another major milestone we're about to reach is the move to a new default
login screen: back in 2022, we decided that user session management on mobile
devices shouldn't get any special treatment, and we should rely on a "display
manager" similar to GDM/LightDM/SDDM, for instance. This would bring many
benefits, the most prominent ones being multi-user setups for those who need it
(e.g. for handing out a personal device for others to try) and automatic
keyring unlocking.

As no mobile-friendly greeter existed back then, we took a stab at hacking
Phosh itself, as it provided almost everything we needed:
* meant to run directly on top of a Wayland compositor
* a lockscreen we could mutate into a login screen
* touch input and on-screen keyboard management

After stripping it from a lot of its functionality and only keeping the
core and lockscreen features, we came up with [phog](https://gitlab.com/mobian1/phog),
which worked so well that we decided to make it part of Mobian (even though
this was supposed to be a mere proof-of-concept), leaving cleanups and
essential refactoring to a later point in time.

While this mostly worked out as expected, it proved to be a maintenance
nightmare, and the combination of time and energy required for this much-needed
refactoring was never found, leaving `phog` in a suboptimal state for the
foreseeable future.

Thankfully, this effort inspired others, and [@samcday](https://github.com/samcday/)
decided to write a new greeter based on the same concepts, this time using
unmodified Phosh code (by using its core functionality as a shared library)
rather than hacking it to pieces. With the help of [@agx](https://honk.sigxcpu.org/con/),
he ensured `libphosh` provided a usable and complete enough API, added Rust
bindings on top of it and finally released [Phrog 🐸](https://github.com/samcday/phrog).

Phrog is now [part of Debian](https://tracker.debian.org/pkg/phrog) and will
be used as [our default greeter](https://salsa.debian.org/Mobian-team/mobian-recipes/-/merge_requests/136)
as soon as it migrates to the `testing` repository. This move should be more
future-proof and further reduce our overall tech debt.

### Supporting new devices in Mobian

On the topic of device-specific tweaks, we made huge progress on ensuring
Qualcomm-based devices can be supported using only packages present in Debian.
As a result, the only [Mobian packages](https://packages.mobian.org/) needed
for those devices are effectively the kernels.

This could be achieved thanks to the combined efforts of many Mobian
contributors, and involved the following tasks:
* package essential userspace services for Debian
  ([qbootctl](https://tracker.debian.org/pkg/qbootctl),
  [qrtr](https://tracker.debian.org/pkg/qrtr),
  [hexagonrpc](https://tracker.debian.org/pkg/hexagonrpc)...) and get them
  to behave as expected, [contributing](https://github.com/linux-msm/qbootctl/pull/1)
  [our](https://github.com/linux-msm/hexagonrpc/pull/2)
  [changes](https://github.com/linux-msm/qrtr/pull/31) upstream
* make existing tweaks as generic as possible and group them into the
  [qcom-phone-utils](https://tracker.debian.org/pkg/qcom-phone-utils) Debian
  package
* import small utilities such as
  [bootmac](https://gitlab.postmarketos.org/postmarketOS/bootmac) and
  [q6voiced](https://gitlab.postmarketos.org/postmarketOS/q6voiced) into
  `qcom-phone-utils`, submitting upstream
  [fixes](https://gitlab.postmarketos.org/postmarketOS/q6voiced/-/merge_requests/6) and
  [improvements](https://gitlab.postmarketos.org/postmarketOS/bootmac/-/merge_requests/12)
  in the process
* improve [droid-juicer](https://gitlab.com/mobian1/droid-juicer) and add
  configurations for new devices so we can keep not worrying about distributing
  proprietary firmware blobs

One of the most significant results of those efforts is of course that new
devices are now supported in Mobian: the Fairphone 5 and Pixel 3a (XL), the
latter having proved to be usable as a daily driver for several people, thanks
to generally-working audio (including phone calls, if you're lucky enough), a
fairly good battery life and kinda-functioning cameras.

Another important improvement is that generating Mobian images for unsupported
(for now) Qualcomm-based devices is now easier than it has ever been (as long
as a mainline-based kernel exists for said devices): using the
[qcom-wip](https://salsa.debian.org/Mobian-team/mobian-recipes/-/merge_requests/102)
device type, one can "simply" drop the downstream kernel's `.deb` package as
well as a `droid-juicer` configuration file to the relevant folders and build
the image, with decent chances that it will actually boot once flashed to the
device.

More detailed instructions are being written and will hopefully be posted to
the [Debian Wiki](https://wiki.debian.org/Mobian/Devices) in the near future.

### On "universal" kernels

As you might remember, we aim for Mobian to ship
[universal images]({{< ref "universal-images.md" >}}), that is, images that can
run on all supported devices. This requires first having a "universal" kernel,
holding all the patches needed for all supported devices. As this is way more
complex than it may sound, we started by offering a single kernel for all
Qualcomm-based devices.

However, when looking into adding support for SDM670-based devices (Pixel 3a),
we faced the fact that some of the patches required for those would conflict
with patches needed for SDM845-based devices. We then had to resolve to
shipping a different kernel for the Pixel 3a, realizing that our plan to ship
a unified kernel would be only get harder to implement as we would add support
for more devices.

More recently, we found out that patches required for SC7280-based devices
(Fairphone 5, and possibly the Nothing Phone in the near future) would break
display support on SDM845 devices, and had to split SC7280 support into its own
separate kernel, putting the final nail in the coffin of our "universal kernel"
dream.

In the end, our formerly-generic
[qcom-linux](https://salsa.debian.org/Mobian-team/devices/kernels/qcom-linux)
kernel now only support SDM845 and SM6350 (Fairphone 4) devices, a situation
only full upstreaming of device support will be able to improve over time.

### What's coming up next?

At this point, we're already pretty happy with how things evolved throughout
2024 and in the first weeks of 2025. Our focus will now be on fixing as many
bugs as possible in preparation for the Trixie release expected next summer.

Unfortunately, we barely made any progress on our
[2024 roadmap]({{< ref "highlights-of-2023.md" >}}#2024-roadmap) items, but
choices had to be made due to the (limited) bandwidth of our developers. This
doesn't mean we no longer have interest in those, but rather that we'll likely
address them only once we have the next stable release out.

On the plus side, Mobian will be participating in this year's "Google Summer Of
Code" through its parent project Debian. Our proposal, aimed at improving
[device tweaks management](https://wiki.debian.org/SummerOfCode2025/ApprovedProjects/DeviceTweaksManagement)
in Debian, has been approved and is only waiting for the right candidate(s),
so feel free to drop us a line and/or join us on
[#mobian-dev:matrix.org](https://matrix.to/#/#mobian-dev:matrix.org) if you're
interested!

Finally, several Mobian developers plan to attend
[DebConf '25](https://debconf25.debconf.org/) in Brest, France, spreading the
Mobile Linux gospel to fellow Debian developers. Maybe we'll get to meet some
of you there as well?
