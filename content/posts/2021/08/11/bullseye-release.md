+++
title = "To Bullseye and beyond!"
date = 2021-08-11T00:00:00+00:00
tags = ["Mobian", "debian", "bullseye", "bookworm", "stable"]
+++

As you might be aware, the next Debian stable release (codename **`bullseye`**)
is only [a few days away](https://lists.debian.org/debian-devel-announce/2021/07/msg00003.html).

But what does it imply for Mobian?

<!--more-->

## Bullseye on mobile

Bullseye will be the first Debian release including a modern software stack
aimed at mobile devices, which is an important milestone in itself. However,
development in this area is progressing at a rapid pace, and most packages
in `bullseye` are already outdated, missing important features such as improved
modem handling during suspend/resume cycles, or recent packages like
[GTK4](https://gtk.org/) and [libadwaita](https://gitlab.gnome.org/GNOME/libadwaita).

As Mobian is developed by a small team of already busy individuals, we have
decided we wouldn't support `bullseye` after it's released and will focus our
efforts on the next Debian version (codename **`bookworm`**).

Current users will still get updates from Debian throughout the lifetime of
`bullseye` but shouldn't expect new versions of user-facing applications or the
mobile software stack. To put it short, here's what you'll get from sticking to
`bullseye`:
- security updates for the core system
- minor upgrades to system packages

Here's what you won't get should you decide to stick to bullseye:
- security updates for [packages maintained by Mobian](https://packages.mobian.org/)
- shiny new features
- usability improvements

## The way forward

Within a few days, following the `bullseye` release, we'll create and populate a
new Mobian repository targeting `bookworm`. Upgrading from `bullseye` to
`bookworm` won't be automated and will require editing your `sources.list`
files: simply replace `bullseye` with `bookworm` in both `/etc/apt/sources.list`
and `/etc/apt/sources.list.d/mobian.list`.

*Notes:*
- *if you originally flashed your device with an image older than April 26th,
  2021, will have to edit `/etc/apt/sources.list.d/extrepo_mobian.sources`,
  replacing `Suites: mobian` with `Suites: bookworm`*
- *if you were using [Mobian staging]({{< ref "unstable-distro.md" >}}) so far,
  you will only need to edit `/etc/apt/sources.list`: our current `staging`
  repo will keep being the entry point for all new and updated packages we
  upload*

Please keep in mind that we expect a huge amount of upgrades to be flowing in
the weeks following the `bullseye` release, with plenty of opportunities to
break the system, so you might want to wait a bit before upgrading ;)

Once the dust settles, we plan to move more and more of Mobian into the Debian
archive with the hope of making Debian `bookworm` a first-class citizen in the
"Linux on mobile" world.

The next couple of years are going to be exciting for all of us, so we can only
thank you for your support, and we hope you'll enjoy the ride at least as much
as we do! :)
