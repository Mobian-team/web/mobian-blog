+++
title = "The PinePhone Pro is here (and it runs Mobian!)"
date = 2021-12-28T00:00:00+00:00
tags = ["mobian", "pinephone", "pro"]
+++

![PinePhone Pro Features](/images/ppp-features.png)

Pine64 [recently announced](https://www.pine64.org/2021/10/15/october-update-introducing-the-pinephone-pro/)
the [PinePhone Pro](https://www.pine64.org/pinephonepro/), a significantly upgraded version
of their [PinePhone](https://www.pine64.org/pinephone/). As developers started receiving
their devices earlier this month, significant progress have been made already to get
existing mobile distributions, including Mobian, to run on this new Linux phone. Keep
reading for the latest status updates on the PinePhone Pro support.

<!--more-->

## Initial hardware enablement

The PinePhone Pro was designed in such a way that it is an easy device to work with: it is
based on a Rockchip RK3399 SoC, for which mainline kernel support is really good, and
switched to a Broadcom/Cypress WiFi/Bluetooth chipset, supported by the mailine `bcrmfmac`
kernel driver. Moreover, the modem is the same model as the one used in the OG PinePhone:
despite its numerous drawbacks, it is a well-known unit and we can already provide
workarounds for its most significant issues.

[@megi](http://xnux.eu/) and [@MartijnBraam](https://mastodon.online/web/@martijnbraam@fosstodon.org),
which both had access to early prototypes of this device, did an awesome job at bringing
up the device and providing patches for u-boot and the Linux kernel. This allowed
distribution developers to boot the PinePhone Pro with very little effort and start
improving on an already solid basis.

As a matter of fact, only a few hours were necessary to craft a Mobian image able to boot
into Phosh on the PinePhone Pro! Making the notification LEDs, flash light and orientation
sensors to work as nicely as they do on the OG PinePhone was only a matter of few more
hours (if not minutes) of additional hacking.

![PinePhone Pro booting Mobian](/images/ppp-first-mobian-boot.png)

## Getting 4G/LTE connectivity

As mentioned above, the PinePhone Pro uses the same exact modem as the OG PinePhone.
As a consequence, only a limited amount of software changes were necessary to get it to
work on Mobian. Using the latest versions of both ModemManager and eg25-manager, the
modem is automatically powered on and able to connect to 4G networks, with both SMS/MMS
and mobile data being fully functional.

## Giving and receiving phone calls

Phone calls are another problem, as those require routing audio properly. A significant
difference with the OG PinePhone is that the modem audio isn't routed through the SoC's
digital audio interface, but instead the modem is connected to a dedicated audio codec.
This codec transforms the digital audio signal coming to/from the modem into/from analog
audio connected to the main audio codec analog in/out ports.

This helps with some of the OG PinePhone issues (audio quality), but also requires
careful attention to the overall audio configuration. Fortunately, the RK3399 audio
interface, as well as the ALC5640 codec, are both very well supported by mainline kernels.
megi had already added the necessary device-tree bits, so all we had to do was adapt
eg25-manager so it configured the modem to use its dedicated audio codec, and write ALSA
UCM profiles for proper audio routing during phone calls.

The latter was the hardest part, although by taking inspiration from the existing UCM
profiles for other devices using the ALC5640 codec, and a few evenings going through the
codec's datasheet and a lot of trial & error, we were able to implement audio routing
for both "normal" use and phone calls, including support for the internal earpiece,
speaker and microphone as well as headphones and headset microphone.

## Reviving cross-distro collaboration

While there always was some level of collaboration between projects for the OG PinePhone,
such effort had somewhat stalled once we had solved the most urgent issues, leading to
downstream kernel patches piling up with only very few of those being successfully
upstreamed.

As an unintended, yet extremely positive, side-effect of the PinePhone Pro release,
developers of most (if not all) mobile Linux distributions have gathered with the goal
of not reproducing such a situation with the PinePhone Pro. We at Mobian are extremely
excited of being part of this effort and look forward to collaborating more closely on
solving hardware support issues and working together on kernel/bootloader development
and upstreaming, and we're sure developers from other projects are as well!

## A new era for Linux phones

The PinePhone Pro is a powerful and nicely designed addition to the existing line of
Linux-first phones. Once its hardware support improves, and with the ever maturing
software ecosystem, it will very likely prove to be a very enjoyable device to use as
a daily driver, making for a much smoother experience than the OG PinePhone.

We're not there yet though, as several major issues still make it a developers-only device
for now:
- power management is really basic at the moment, and resuming from deep sleep still causes
  issues with the modem not being brought back properly, or the audio sample rate not being
  configured as it should (resulting in sped up, high-pitched audio)
- the on-device installer mostly works when installing from an SD card to the same SD card,
  but encrypted installs won't boot due to an initramfs issue
- the charger and battery drivers present a number of problems as well
- Bluetooth headsets can't be used during phone calls
- display output through USB-C doesn't work yet
- camera support is basically non-existent at this point

We're quite confident most, if not all of those issues will be solved eventually, and a
lot of us will end up being able to daily drive the PinePhone Pro before 2022 ends, so
stay tuned as we will undoubtedly make great progress in the months to come! :)

In the meantime, for those of you lucky enough to already own a PinePhone Pro, make sure
to check out [Mobian weekly images](https://images.mobian.org/pinephonepro/weekly/)
for this device.
