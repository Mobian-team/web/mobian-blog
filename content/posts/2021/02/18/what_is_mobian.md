+++
title = "What is Mobian?"
date = 2021-02-18T00:00:00+00:00
tags = ["Mobian"]
+++

Now that the Mobian CE is being sold, and Mobian becomes more popular,
we are often asked in the support channel, "__what and who is
Mobian?__" In the midst of action, we don't reflect upon this subject
very often, but it is worth contemplating and summarizing.

<!--more-->

## What is Mobian?

Mobian aims to integrate the standard [Debian](https://www.debian.org)
distribution with Phone-specific projects and modifications in a
distribution that works on certain mobile phones and tablets, such as
the Pinephone, the Pinetab and the Librem 5. The idea is to minimize
the Mobian specific pieces by “upstreaming” changes to the original
projects as much as possible.

To achieve this, we tread a delicate balance between carrying
customized patches and packages that are required to make Mobian run
well on its supported devices, while trying to push as many of these
changes into upstream Debian. You could think of Mobian as a [Debian
Pure Blend](https://www.debian.org/blends/), and indeed we aim at
becoming one at some point in time.

For the time being, we are focused on the same software stack that
Purism uses for the Librem5, that is, wayland-ish, gnome-ish, modem
manager-ish. Being based on the Phosh environment facilitates that,
but of course it is perfectly possible to run Qt-based software. We
have nothing against KDE and the plasma shell, and are contemplating
whether, when and how we can support that too, but for the time being
creating a single working GUI is enough of a challenge.

## Who is Mobian?

It was founded by *a-wai* in order to make Debian running nicely on
the Pinephone. Mobian's public outing occured on 14 May 2020 when
Mobian was announced in [this
thread](https://forum.pine64.org/showthread.php?tid=9850) in the
PINE64.org forum.

a-wai is the founder and leader of the project. That having said, he
has proven to be listening and acting upon the opinions of his fellow
volunteers[^awai]. These volunteers are a loose and informal
collection of people that have nagged @a-wai long enough until he
invited them into the (not so secret) developers room, where they
conspire to achieve world domination. All developers are unpaid
volunteers, enthusiastic, nice & friendly[^friendly].

We are not associated with a single nation, company or timezone[^EST].

[^awai]: @a-wai has not written this sentence. He has not been asked
    and will not be asked whether he would support that statement :-).
[^friendly]: Please treat them in a manner they remain so.
[^EST]: No, we are not part of the [Eastern Standard Tribe](https://en.wikipedia.org/wiki/Eastern_Standard_Tribe)).

### How much are you earning with Mobian?

If you [donate](https://liberapay.com/mobian) to Mobian via liberapay,
you'll see on that page what the project currently receives. Of the
sold Mobian CE phones, [PINE64](https://pine64.com) donates us 10$ per
phone (indeed: WOW! and THX!). We are also grateful to being able to
use the services of gitlab and salsa.debian.org.

The money is being used to pay for our web hosting (server and bandwidth)
and hardware we need for development purpose (new devices, build machines,
useful accessories...). Part of the user donations also cover some of the
beer expenses of our developers so they don't have to fight thirst while
coding. In the future, we might use also some of those funds to sponsor
needy developers with a device to enable hacking (don't start asking us
for free devices for now, thank you).

### How to help us

Use it, improve it, both the software and the documentation. As we
depend upon software in the Debian ecosystem, you don't even need to
have supported hardware to make an app suitable for small screens, or
implement a feature or application that is need to make the experience
smoother.

### Standing on the shoulders of ...

We would not be where we are without the Debian ecosystem or
[Purism](https://puri.sm) and their pioneering efforts to make Linux
usable on phones. We are also grateful to PINE64 for selling a [Mobian
community edition]({{< ref "mobian-community-edition.md" >}}) which
allowed us to place the Debian swirl onto people's phones, and for donating
per sold community edition device to the Mobian project.  We are also
grateful to [PostmarketOS](https://postmarketos.org/) for a number of
userspace programs and last but not least all the kernel hackers
(Ondrej Jirman aka megous and Samuel Holland) hacking away and
improving hardware support.

### Project FAQ

##### What is your code of conduct, what is your privacy policy, what is your...

Given that we are closely aligned with the Debian project, have a look
at their [code of conduct](https://www.debian.org/code_of_conduct),
contribution guidelines, etc and you get a pretty good picture.

##### But is Android not already Linux on mobile?

We could answer this question. However, it would not be printable on
the public Interwebs, so you won't find the answer in this post.

##### Can I pay £|$|€|... to implement feature "X"?

Do hire or donate to someone to implement *X* directly instead of
channeling money through us. It might help to ask beforehand whether
we (or Debian) would like to have feature *X*, though.
