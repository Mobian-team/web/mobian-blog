+++
title = "Announcing the PinePhone Mobian Community Edition"
date = "2021-01-15"
tags = ["mobian", "pinephone", "pine64"]
+++

![Mobian Community Edition](/images/mobian-ce-header.jpg)

We are absolutely thrilled to announce that [Pine64](https://www.pine64.org)
is releasing a Mobian Community Edition of the
[PinePhone](https://www.pine64.org/pinephone/)! This is a major milestone for
our project, which started less than a year ago as a way to bring Debian to this
exact mobile device!

<!--more-->

# A Debian computer in your pocket

The PinePhone is basically a handheld computer with 4G connectivity. With
[Mobian](https://www.mobian.org/), this (truly smart) phone comes with
a distribution tons of [Debian](https://www.debian.org) users are already
familiar with, optimized for the PinePhone while still giving them access to the
50,000+ packages already in the Debian archive.

Mobian ships with [Phosh](https://puri.sm/posts/phosh-overview/), a
mobile-oriented desktop environment based on [GNOME](https://www.gnome.org/)
technology, initially developed by [Purism](https://puri.sm/) for their
[Librem 5](https://puri.sm/products/librem-5/) smartphone. Default applications
have been selected for their usability on a phone-size display wherever
possible, and we're confident this list can only grow over time.

Combined with the absence of software trackers, this gives you an ad-free,
privacy-respecting platform you can bring anywhere with you and use the same way
you do with your personal computer. Taking it to the next level, plug in a USB-C
dock connected to a keyboard, mouse and HDMI monitor, and you won't be able to
tell if you're using a phone or a "regular" computer.

# What to expect

When first powering on your PinePhone Mobian Community Edition, you'll be
greeted with an installer, allowing you to choose your user password and
presenting you with the ability to enable full disk encryption.

Hardware support is pretty much complete, and while it still needs to be refined
in a number of areas, you'll be able to enjoy full phone functionality
(including voice calls, SMS, mobile broadband, WiFi, front and back cameras...)
from day one.

However, please note the software is still mostly work-in-progress, and evolves
on an almost-daily basis. Your device will arrive with a several months old
image, so we can only recommend that you update your PinePhone as soon as
possible to take advantage of the latest developments.

# Pricing & Availability

The PinePhone Mobian Community Edition will be available to pre-order from the
[Pine64 Store](https://pine64.com/product-category/pinephone/) on January 18th
and comes in two configurations:

* $149 – 2GB RAM + 16GB eMMC
* $199 – 3GB RAM + 32GB eMMC Convergence Package with an included USB-C dock

Finally, you should be aware of the fact that Pine64 will generously donate $10
to the Mobian project for each device sold, a significant portion of which will
be given back to the Debian project on which we rely heavily (and to which we
obviously contribute!). We can't thank them enough for their commitment to bring
FLOSS-driven devices to the market and support to open-source projects!
