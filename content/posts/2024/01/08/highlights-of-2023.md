+++
title = "Highlights of 2023"
date = 2024-01-14T00:00:00+00:00
tags = ["mobian", "overview", "roadmap"]
+++

Time flies when you are having fun. 2023 was no exception, and we find ourselves in 2024 already! Time to take stock what happened last year (and perhaps also what we attempt to achieve this year).
<!--more-->

### Bookworm release
It is safe to say that the [bookworm release](https://blog.mobian.org/posts/2023/06/10/bookworm/) was a major milestone for Mobian. For the first time, we have a stable release that we find to actually be useful on a mobile screen and phone. Putting out stable images was quite some work but we are proud of it.

We had decided to move on and focus on the next development cycle rather than starting to backporting features and releases. So bookworm continues to receive security updates, but no new features. Still, it might be a useful stable base for people who do not like to break their phone that often. By using flatpak and other means, one can even use actual releases of apps and not miss out on the latest hotness.

### App ecosystem

The ecosystem of applications suitable for mobile screen continued to grow in 2023, filling many important functionality gaps. That includes core GNOME apps included in the distribution that became ported to work better on small screens, to an external ecosystem of applications that can be installed via flatpak or other means. Whether you want to [track your jogging tracks](https://flathub.org/apps/xyz.slothlife.Jogger), travel via [public transportation](https://packages.debian.org/trixie/railway-gtk), or [toot on social media](https://packages.debian.org/trixie/tuba), many puzzle pieces start to fall into place.

https://linuxphoneapps.org/ gives you a great overview of what exists within and outside of distributions. (and yes, there is active Debianization of some apps going on, such as the "railway" application which went into Debian just now.

### Shrinking Mobian

Despite all that growth we managed to shrink the packages that are in the [mobian repository](https://packages.mobian.org/) even more over time. As you know, Mobian is nothing but a thin overlay over stock Debian (the regular Debian repository with its updates is a core pillar of Mobian), and it is declared goal of Mobian to abolish itself by upstreaming all deviations and changes to Debian proper.

This work has continued in 2023, and many of the Mobian developers are also [official](https://lists.debian.org/debian-project/2022/06/msg00019.html) [Debian](https://nm.debian.org/person/devrtz/) [developers](https://nm.debian.org/person/undef/) and maintainers, continuing to maintain packages in Debian.

While we might never manage to cease to exist, we are making sure that Mobian is as slim as possible and as aligned to Debian and its core values as possible.

### FOSDEM 2023 & Distro Collaborations

The biggest true community event in the FOSS world is undoubtedly FOSDEM, and we were very much present there in 2023. It was the first time that many developers got to see each other for the first time. Nothing bonds as much as the collaborative intake of beverages of ones choice.

Also, we had a very successful developer room on "FOSS on Mobile devices" which showed the incredible interest in topics close to our heart. In 2024, there will even be a full-day developer room on the same topic, check [the schedule](https://fosdem.org/2024/schedule/track/foss-on-mobile-devices/) in case you are interested.

Another thing happening at FOSDEM is that Mobian people got to know developers of other projects much better. Notably, meeting people working on PureOS (and Purism support) as well as postmarketOS developers was quite interesting and fun. This led to collaboration and friendship. And hey, Mobian developers were extensively featured in a [postmarketOS podcast episode](https://cast.postmarketos.org/episode/35-Interview-a-wai-devrtz-kop316-Mobian/) if you need any audible proof of that! We also shared a stand with many other distros and way too many devices on too little cramped space. The resulting mayhem was messy, creative and fun and we were overwhelmed by the interest of the FOSDEM public. It very much showed how many ~~competing~~ organizations can work together to achieve a common goal.

Perhaps this is the right time to give a shoutout to PureOS and pmOS anyway, these people are helping to push towards a common (public) good. PureOS publishes its patches on top of upstream projects in a format that allows others to easily adopt them and actively pushes changes upstream. pmOS is super helpful with a "fix upstream first" attitude, so that everyone benefits from fixes. We hope to continue to have such a great time with our "competition".

### Environment friendly

While phosh and many of the GNOME ecosystem applications are the default environment that we rely on, we would like to cater to fans of other environments as well. As such it is great to see that progress has been made for both the Plasma Mobile and SXMO packaging. With support of other Debian developers, both environments are usable and selectible from the login screen in Mobian. There are still rough edges, but we are slowly getting there.

### Fresh kernels!

We managed to rebase the kernel for our supported hardware to Linux 6.6 in Trixie. This was not an easy task, giving all the ongoing work. Especially on sunxi64 (the OG pinephone kernel) it needed lots of work. Shoutout to Andrey Skvortsov for his efforts!

If you're on staging you may have also noticed new kernels coming through much more regularly. 
After the 1000th time rebasing on an upstream stable release we decided that the mix of manual work
and ad-hoc scripts we had should be made official, and where better than our own Mobot!

Now each upstream release is automatically downloaded, verified, patched and released directly to staging,
allowing Mobian developers to focus on other things, like packaging applications and upstreaming these patches away.

## 2024 Roadmap

Now that we have looked at our accomplishements of the last year, it is worth peeking into our crystal ball to see what we would love to do in 2024. There is another release freeze looming at the end of 2024. So basically, what we achieve in 2024 will determine how Trixie is going to look like.

Some of the things we are aiming for are:

- A runtime-based tweaks management
 
  Which would allow us to use a unified image and packages for hardware with different requirements and configurations.

- Get all device-agnostic packages into Debian (tweaks, meta...); use Debian kernel wherever possible

  Reduce the mobian-specific footprint even more. This might not be feasible for the kernel of all hardware, but perhaps for some.

- Improve upstream PipeWire & WirePlumber for call audio

  Pipewire is the future and Wireplumber likely needs to do some of the things we have hacked up callaudiod for now.

- More upstreaming of patches are eg. in the area of audio alsa UCM configs and kernel patches.

- It would be awesome to create a single installer image for all Linux-first devices, so we don't have to create installers for each device.

- Lastly, the on-screen keyboard osk-sdl needs to be fully and cleanly replaced by unl0kr with a good transition path between these.

These are all lofty plans though, and will obviously depend on (wo)manpower, spare time and external factors and developments. So no guarantees will be made. One needs to have goals though...

Happy 2024 everyone, let's keep rocking the thing!
