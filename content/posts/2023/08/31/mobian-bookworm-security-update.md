+++
title = "Mobian Security Update #1"
date = 2023-08-31T02:00:00+00:00
tags = ["mobian", "security"]
+++

The Mobian Team have recently released a security update fixing a **local** vulnerability caused 
by a bug in our Bookworm images. 

### User Action Required

Mobian users must install the following package in order to address the issues described in this post.
A normal upgrade which can be done using `sudo apt update && sudo apt upgrade` or via Gnome Software
will take care of this.

* For Bookworm users:
    *  `mobian-tweaks-common`, version 0.5.6+deb12u1
* For Trixie and Staging users:
    * `mobian-tweaks-common`, version 0.5.9

These packages include an install script which fixes the permissions on the effected files. They are currently 
in the `staging` and `bookworm-updates` repositories and will migrate to `trixie` and `bookworm`
in the coming week.

### Summary

The Bookworm images released earlier this year had a small number of files with global
write permissions. As two of these files manage the configuration of the `apt` package 
manager, they provide an attacker with a useful privilege escalation method.

This issue seems to only affect the released Bookworm images, though a full audit of weekly
images has not been undertaken. Users who installed on Trixie were likely never effected.

### Impact

These files could allow a local user to gain root access to the device
by modifying them and waiting for the user to install an attacker provided update. This attack
would require access to an unlocked or otherwise already compromised device as a standard user.

There is no way to carry this attack out remotely or on a locked device.

### Indicators of Compromise
Users can verify the current configuration of their system by checking the relevant files.
Changes to these files to not immediately mean the device has been compromised, but may warrant
further investigation.

`/etc/apt/sources.list.d/mobian.list` should contain (potentially with a different distribution):
```
deb http://repo.mobian.org/ <distribution> main non-free-firmware
```

`/etc/apt/trusted.gpg.d/mobian.gpg` can be verified using the command `shasum -c checksum.txt` aganist the following checksum file:
```
4ab90ff82a88f11f681e5e857503833eb2108c9a77edaa9f64b7648c1b91c60a  /etc/apt/trusted.gpg.d/mobian.gpg
```

### Credit

Our thanks go to @Ove for discovering and reporting the issue.
